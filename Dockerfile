#FROM node:16 AS build
#WORKDIR /app
#COPY package*.json ./
#RUN npm install
#COPY . .
##UN ng build

FROM nginx:alpine
COPY dist/appsmith-container/browser /usr/share/nginx/html
